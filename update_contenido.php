<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));
$contenido = $data->contenido;
$navegacion = $data->navegacion;
$conn = $db_connection->dbConnection($data->db);
//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

echo json_encode($data->navegacion->navegacion_id);

echo json_encode($data->contenido->contenido);


//CHECKING, IF ID AVAILABLE ON $data
if(isset($data->navegacion->navegacion_id)){
    $post_id = $data->navegacion->navegacion_id;
    //GET POST BY ID FROM DATABASE
    $get_post = "SELECT * FROM `kh_navegacion` WHERE id=:post_id";
    $get_stmt = $conn->prepare($get_post);
    $get_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);
    $get_stmt->execute();
    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($get_stmt->rowCount() > 0){
        // FETCH POST FROM DATBASE 
        $row = $get_stmt->fetch(PDO::FETCH_ASSOC);
        // CHECK, IF NEW UPDATE REQUEST DATA IS AVAILABLE THEN SET IT OTHERWISE SET OLD DATA
        $post_parent_id = isset($navegacion->parent_id) ? $navegacion->parent_id : $row['parent_id'];
        $post_navegacion = isset($navegacion->navegacion) ? $navegacion->navegacion : $row['navegacion'];
        $post_tipo_vista_id = isset($navegacion->tipo_vista_id) ? $navegacion->tipo_vista_id : $row['tipo_vista_id'];
        $post_tipo_destino_id= isset($navegacion->tipo_destino_id) ? $navegacion->tipo_destino_id : $row['tipo_destino_id'];
        $post_imagen_entrada= isset($navegacion->imagen_entrada) ? $navegacion->imagen_entrada : $row['imagen_entrada'];
        $update_query = "UPDATE `kh_navegacion` SET parent_id = :parent_id, navegacion = :navegacion, tipo_vista_id = :tipo_vista_id, tipo_destino_id = :tipo_destino_id, imagen_entrada = :imagen_entrada
        WHERE id = :post_id";
        $update_stmt = $conn->prepare($update_query);
        // DATA BINDING AND REMOVE SPECIAL CHARS AND REMOVE TAGS
        $update_stmt->bindValue(':parent_id', $post_parent_id,PDO::PARAM_STR);
        $update_stmt->bindValue(':navegacion', $post_navegacion,PDO::PARAM_STR);
        $update_stmt->bindValue(':tipo_vista_id', $post_tipo_vista_id,PDO::PARAM_STR);
        $update_stmt->bindValue(':tipo_destino_id', $post_tipo_destino_id,PDO::PARAM_STR);
        $update_stmt->bindValue(':imagen_entrada', $post_imagen_entrada,PDO::PARAM_STR);        
        $update_stmt->bindValue(':post_id', $post_id,PDO::PARAM_STR);
        if($update_stmt->execute()){
            $msg['message'] = 'Datos actualizados correctamente';
        }else{
            $msg['message'] = 'data not updated';
        }
    }else{
        $msg['message'] = 'Invalid ID';
    }
}



if(isset($data->navegacion->navegacion_id)){
    $post_id = $data->navegacion->navegacion_id;
    //GET POST BY ID FROM DATABASE
    $get_post = "SELECT * FROM `kh_contenido` WHERE navegacion_id=:post_id";
    $get_stmt = $conn->prepare($get_post);
    $get_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);
    $get_stmt->execute();
    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($get_stmt->rowCount() > 0){
        // FETCH POST FROM DATBASE 
        $row = $get_stmt->fetch(PDO::FETCH_ASSOC);
        // CHECK, IF NEW UPDATE REQUEST DATA IS AVAILABLE THEN SET IT OTHERWISE SET OLD DATA
        $post_contenido= isset($data->contenido->contenido) ? $data->contenido->contenido : $row['contenido'];
        $update_query = "UPDATE `kh_contenido` SET  contenido = :contenido
        WHERE navegacion_id = :post_id";
        $update_stmt = $conn->prepare($update_query);
        // DATA BINDING AND REMOVE SPECIAL CHARS AND REMOVE TAGS
        $update_stmt->bindValue(':contenido', $post_contenido,PDO::PARAM_STR);
        $update_stmt->bindValue(':post_id', $post_id,PDO::PARAM_STR);
        if($update_stmt->execute()){
            $msg['message'] = 'Datos actualizados correctamente';
        }else{
            $msg['message'] = 'data not updated';
        }
    }else{
        $msg['message'] = 'Invalid ID';
    }
}


//     $check_post = "SELECT * FROM `kh_contenido` WHERE navegacion_id=:post_id";
//     $check_post_stmt = $conn->prepare($check_post);
//     $check_post_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);
//     $check_post_stmt->execute();

//     if($get_stmt->rowCount() > 0){
//         $post_contenido = isset($contenido->contenido) ? $contenido->contenido : $row['contenido'];
//         $post_tipo_contenido_id= isset($contenido->tipo_contenido_id) ? $contenido->tipo_contenido_id : $row['tipo_contenido_id'];
//         $update_query = "UPDATE `kh_contenido` SET contenido = :contenido, tipo_contenido_id = :tipo_contenido_id
//         WHERE navegacion_id = :post_id";

//         $insert_stmt = $conn->prepare($insert_query);
//         // DATA BINDING
//         $insert_stmt->bindValue(':contenido', $contenido->contenido,PDO::PARAM_STR);
//         $insert_stmt->bindValue(':tipo_contenido_id', htmlspecialchars(strip_tags($contenido->tipo_contenido_id)),PDO::PARAM_STR);

//         if($insert_stmt->execute()){
//             $msg['message_contenido'] = $contenido;
//         }else{
//             $msg['message_contenido'] = 'Data not Inserted';
//         }

// }else{
//     // $msg['message_contenido'] = 'Please fill all the fields | Contenidos';
//     $msg['message_contenido'] = $data;
// }



?>