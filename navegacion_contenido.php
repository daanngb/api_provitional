<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if(isset($data->db)) {
    // DEFINE DATABSE
    $conn = $db_connection->dbConnection($data->db);
} else {
    $msg['message'] = 'NO DATABASE DEFINED';
    echo $msg;
    exit();
}

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($data->parent_id) && isset($data->navegacion) && isset($data->tipo_vista_id) && isset($data->tipo_destino_id) && isset($data->imagen_entrada) && isset($data->usuario_creacion_id)){

        $insert_query = "INSERT INTO `kh_navegacion`(parent_id,navegacion,tipo_vista_id,tipo_destino_id,imagen_entrada,usuario_creacion_id) VALUES(:parent_id,:navegacion,:tipo_vista_id,:tipo_destino_id,:imagen_entrada,:usuario_creacion_id)";

        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':parent_id', htmlspecialchars(strip_tags($data->parent_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':navegacion', htmlspecialchars(strip_tags($data->navegacion)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipo_vista_id', htmlspecialchars(strip_tags($data->tipo_vista_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipo_destino_id', htmlspecialchars(strip_tags($data->tipo_destino_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':imagen_entrada', htmlspecialchars(strip_tags($data->imagen_entrada)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':usuario_creacion_id', htmlspecialchars(strip_tags($data->usuario_creacion_id)),PDO::PARAM_STR);

        if($insert_stmt->execute()){
            $msg['message'] = $data;
        }else{
            $msg['message'] = 'Data not Inserted';
        }

}else{
    $msg['message'] = 'Please fill all the fields | Navegacion Contenidos';
}

//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>