<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));
$contenido = $data->contenido;
$navegacion = $data->navegacion;

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if(isset($data->db)) {
    // DEFINE DATABSE
    $conn = $db_connection->dbConnection($data->db);
} else {
    $msg['message'] = 'NO DATABASE DEFINED';
    echo $msg;
    exit();
}

// CREATE NAVIGATION
// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($navegacion->parent_id) && isset($navegacion->navegacion) && isset($navegacion->tipo_vista_id) && isset($navegacion->tipo_destino_id) && isset($navegacion->imagen_entrada) && isset($navegacion->usuario_creacion_id)){

    $insert_query = "INSERT INTO `kh_navegacion`(parent_id,navegacion,tipo_vista_id,tipo_destino_id,imagen_entrada,usuario_creacion_id) VALUES(:parent_id,:navegacion,:tipo_vista_id,:tipo_destino_id,:imagen_entrada,:usuario_creacion_id)";

    $insert_stmt = $conn->prepare($insert_query);
    // DATA BINDING
    $insert_stmt->bindValue(':parent_id', htmlspecialchars(strip_tags($navegacion->parent_id)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':navegacion', htmlspecialchars(strip_tags($navegacion->navegacion)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':tipo_vista_id', htmlspecialchars(strip_tags($navegacion->tipo_vista_id)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':tipo_destino_id', htmlspecialchars(strip_tags($navegacion->tipo_destino_id)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':imagen_entrada', htmlspecialchars(strip_tags($navegacion->imagen_entrada)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':usuario_creacion_id', htmlspecialchars(strip_tags($navegacion->usuario_creacion_id)),PDO::PARAM_STR);

    if($insert_stmt->execute()){
        $msg['message_navegacion'] = $navegacion;
        $navegacion_id = $conn->lastInsertId();
    }else{
        $msg['message_navegacion'] = 'NAVIGATION not Inserted';
        echo  json_encode($msg);
        exit();
    }

}else{
    $msg['message_navegacion'] = 'Please fill all the fields | Navegacion';
    echo  json_encode($msg);
    exit();
}

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message_contenido'] = '';

//CREATE CONTENIDO
if(isset($navegacion_id) && isset($contenido->contenido) && isset($contenido->tipo_contenido_id) && isset($contenido->usuario_creacion_id)){

        $insert_query = "INSERT INTO `kh_contenido`(navegacion_id,contenido,tipo_contenido_id,usuario_creacion_id) VALUES(:navegacion_id,:contenido,:tipo_contenido_id,:usuario_creacion_id)";

        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':navegacion_id', htmlspecialchars(strip_tags($navegacion_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':contenido', $contenido->contenido,PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipo_contenido_id', htmlspecialchars(strip_tags($contenido->tipo_contenido_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':usuario_creacion_id', htmlspecialchars(strip_tags($contenido->usuario_creacion_id)),PDO::PARAM_STR);

        if($insert_stmt->execute()){
            $msg['message_contenido'] = $contenido;
        }else{
            $msg['message_contenido'] = 'Data not Inserted';
        }

}else{
    // $msg['message_contenido'] = 'Please fill all the fields | Contenidos';
    $msg['message_contenido'] = $data;
}

//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>