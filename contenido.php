<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($data->navegacion_id) && isset($data->contenido) && isset($data->tipo_contenido_id) && isset($data->usuario_creacion_id)){

        $insert_query = "INSERT INTO `kh_contenido`(navegacion_id,contenido,tipo_contenido_id,usuario_creacion_id) VALUES(:navegacion_id,:contenido,:tipo_contenido_id,:usuario_creacion_id)";

        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':navegacion_id', htmlspecialchars(strip_tags($data->navegacion_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':contenido', htmlspecialchars(strip_tags($data->contenido)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipo_contenido_id', htmlspecialchars(strip_tags($data->tipo_contenido_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':usuario_creacion_id', htmlspecialchars(strip_tags($data->usuario_creacion_id)),PDO::PARAM_STR);

        if($insert_stmt->execute()){
            $msg['message'] = $data;
        }else{
            $msg['message'] = 'Data not Inserted';
        }

}else{
    $msg['message'] = 'Please fill all the fields | Contenidos';
}

//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>