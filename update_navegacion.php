<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));
$conn = $db_connection->dbConnection($data->db);
//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';


//CHECKING, IF ID AVAILABLE ON $data
if(isset($data->id)){
    $post_id = $data->id;
    //GET POST BY ID FROM DATABASE
    $get_post = "SELECT * FROM `kh_navegacion` WHERE id=:post_id";
    $get_stmt = $conn->prepare($get_post);
    $get_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);
    $get_stmt->execute();
    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($get_stmt->rowCount() > 0){
        // FETCH POST FROM DATBASE 
        $row = $get_stmt->fetch(PDO::FETCH_ASSOC);
        // CHECK, IF NEW UPDATE REQUEST DATA IS AVAILABLE THEN SET IT OTHERWISE SET OLD DATA
        $post_navegacion = isset($data->navegacion) ? $data->navegacion : $row['navegacion'];
        $post_tipo_vista_id = isset($data->tipo_vista_id) ? $data->tipo_vista_id : $row['tipo_vista_id'];
        $post_tipo_destino_id = isset($data->tipo_destino_id) ? $data->tipo_destino_id : $row['tipo_destino_id'];
        $post_imagen_entrada = isset($data->imagen_entrada) ? $data->imagen_entrada : $row['imagen_entrada'];
        $update_query = "UPDATE `kh_navegacion` SET navegacion = :navegacion, tipo_vista_id = :tipo_vista_id, tipo_destino_id = :tipo_destino_id, imagen_entrada = :imagen_entrada
        WHERE id = :post_id";
        $update_stmt = $conn->prepare($update_query);
        // DATA BINDING AND REMOVE SPECIAL CHARS AND REMOVE TAGS
        $update_stmt->bindValue(':navegacion', $post_navegacion,PDO::PARAM_STR);
        $update_stmt->bindValue(':tipo_vista_id', $post_tipo_vista_id,PDO::PARAM_STR);
        $update_stmt->bindValue(':tipo_destino_id', $post_tipo_destino_id,PDO::PARAM_STR);
        $update_stmt->bindValue(':imagen_entrada', $post_imagen_entrada,PDO::PARAM_STR);        
        $update_stmt->bindValue(':post_id', $post_id,PDO::PARAM_STR);
        if($update_stmt->execute()){
            $msg['message'] = 'Datos actualizados correctamente';
        }else{
            $msg['message'] = 'data not updated';
        }
    }else{
        $msg['message'] = 'Invalid ID';
    }
    //ECHO DATA IN JSON FORMAT
    echo  json_encode($msg);
}
?>