<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: DELETE");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if(isset($data->db)) {
    // DEFINE DATABSE
    $conn = $db_connection->dbConnection($data->db);
} else {
    $msg['message'] = 'NO DATABASE DEFINED';
    echo $msg;
    exit();
}

//CHECKING, IF ID AVAILABLE ON $data
if(isset($data->id)){

    $post_id = $data->id;

    //GET POST BY ID FROM DATABASE
    // YOU CAN REMOVE THIS QUERY AND PERFORM ONLY DELETE QUERY
    $check_post = "SELECT * FROM `kh_navegacion` WHERE id=:post_id";
    $check_post_stmt = $conn->prepare($check_post);
    $check_post_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);
    $check_post_stmt->execute();

    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($check_post_stmt->rowCount() > 0){

        //DELETE POST BY ID FROM DATABASE
        $delete_post = "DELETE FROM `kh_navegacion` WHERE id=:post_id";
        $delete_post_stmt = $conn->prepare($delete_post);
        $delete_post_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);

        if($delete_post_stmt->execute()){
            $msg['message'] = 'Element Deleted Successfully';
        }else{
            $msg['message'] = 'Element Not Deleted';
        }

    }else{
        $msg['message'] = 'Invlid ID';
    }
    // ECHO MESSAGE IN JSON FORMAT
    echo  json_encode($msg);

}
?>