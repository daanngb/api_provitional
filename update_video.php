<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));
$conn = $db_connection->dbConnection($data->db);
//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';


//CHECKING, IF ID AVAILABLE ON $data
if(isset($data->id)){
    $post_id = $data->id;
    //GET POST BY ID FROM DATABASE
    $get_post = "SELECT * FROM `kh_multimedia` WHERE id=:post_id";
    $get_stmt = $conn->prepare($get_post);
    $get_stmt->bindValue(':post_id', $post_id,PDO::PARAM_INT);
    $get_stmt->execute();
    //CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
    if($get_stmt->rowCount() > 0){
        // FETCH POST FROM DATBASE 
        $row = $get_stmt->fetch(PDO::FETCH_ASSOC);
        // CHECK, IF NEW UPDATE REQUEST DATA IS AVAILABLE THEN SET IT OTHERWISE SET OLD DATA
        $post_navegacion_id = isset($data->navegacion_id) ? $data->navegacion_id : $row['navegacion_id'];
        $post_tipo_multimedia = isset($data->tipo_multimedia) ? $data->tipo_multimedia : $row['tipo_multimedia'];
        $post_multimedia= isset($data->multimedia) ? $data->multimedia : $row['multimedia'];
        $post_imagen_entrada= isset($data->imagen_entrada) ? $data->imagen_entrada : $row['imagen_entrada'];
        $post_url_recurso= isset($data->url_recurso) ? $data->url_recurso : $row['url_recurso'];
        $update_query = "UPDATE `kh_multimedia` SET navegacion_id = :navegacion_id, tipo_multimedia = :tipo_multimedia, multimedia = :multimedia, imagen_entrada = :imagen_entrada, url_recurso =:url_recurso
        WHERE id = :post_id";
        $update_stmt = $conn->prepare($update_query);
        // DATA BINDING AND REMOVE SPECIAL CHARS AND REMOVE TAGS
        $update_stmt->bindValue(':navegacion_id', $post_navegacion_id,PDO::PARAM_STR);
        $update_stmt->bindValue(':tipo_multimedia', $post_tipo_multimedia,PDO::PARAM_STR);
        $update_stmt->bindValue(':multimedia', $post_multimedia,PDO::PARAM_STR);
        $update_stmt->bindValue(':imagen_entrada', $post_imagen_entrada,PDO::PARAM_STR);
        $update_stmt->bindValue(':url_recurso', $post_url_recurso,PDO::PARAM_STR);        
        $update_stmt->bindValue(':post_id', $post_id,PDO::PARAM_STR);
        if($update_stmt->execute()){
            $msg['message'] = 'Video actualizado correctamente';
        }else{
            $msg['message'] = 'data not updated';
        }
    }else{
        $msg['message'] = 'Invalid ID';
    }
    //ECHO DATA IN JSON FORMAT
    echo  json_encode($msg);
}
?>