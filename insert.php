<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

if(isset($data->db)) {
    // DEFINE DATABSE
    $conn = $db_connection->dbConnection($data->db);
} else {
    $msg['message'] = 'NO DATABASE DEFINED';
    echo $msg;
    exit();
}

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($data->navegacion_id) && isset($data->tipo_multimedia) && isset($data->multimedia) && isset($data->imagen_entrada) && isset($data->url_recurso) && isset($data->usuario_creacion_id)){

   // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($data->navegacion_id) && !empty($data->tipo_multimedia) && !empty($data->multimedia) && !empty($data->imagen_entrada) && !empty($data->url_recurso) && !empty($data->usuario_creacion_id)){

        $insert_query = "INSERT INTO `kh_multimedia`(navegacion_id,tipo_multimedia,multimedia,imagen_entrada,url_recurso,usuario_creacion_id) VALUES(:navegacion_id,:tipo_multimedia,:multimedia,:imagen_entrada,:url_recurso,:usuario_creacion_id)";

        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':navegacion_id', htmlspecialchars(strip_tags($data->navegacion_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':tipo_multimedia', htmlspecialchars(strip_tags($data->tipo_multimedia)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':multimedia', htmlspecialchars(strip_tags($data->multimedia)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':imagen_entrada', htmlspecialchars(strip_tags($data->imagen_entrada)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':url_recurso', htmlspecialchars(strip_tags($data->url_recurso)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':usuario_creacion_id', htmlspecialchars(strip_tags($data->usuario_creacion_id)),PDO::PARAM_STR);

        if($insert_stmt->execute()){
            $msg['message'] = $data;
        }else{
            $msg['message'] = 'Data not Inserted';
        }

    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }

}else{
    $msg['message'] = 'Please fill all the fields | navegacion_id,tipo_multimedia,multimedia,imagen_entrada,url_recurso,usuario_creacion_id';
}

//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>
